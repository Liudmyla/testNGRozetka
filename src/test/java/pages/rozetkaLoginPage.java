package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.codeborne.selenide.Selenide.$;

public class rozetkaLoginPage extends basePage {

    @FindBy(xpath = "//input[@name='login']")
    private WebElement loginField;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement passField;

    @FindBy(xpath = "//*[@name = 'auth_submit']")
    private WebElement submitBtn;


    public rozetkaLoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void loginSet(String loginData) {
        $(loginField).clear();
        $(loginField).sendKeys(loginData);
    }

    public void passSet(String passData) {
        $(passField).clear();
        $(passField).sendKeys(passData);
    }

    public void submitBtnClick() {
        $(submitBtn).click();
    }
}
