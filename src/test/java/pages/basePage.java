package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.codeborne.selenide.Selenide.$;

public class basePage {

    WebDriver driver;

    @FindBy(xpath = "//*[@name = 'signin']")
    private WebElement loginPageLink;

    basePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void loginPageOpen() {
        $(loginPageLink).click();
    }

    public boolean loginPageFound() {
        return $(loginPageLink).exists();
    }

}
