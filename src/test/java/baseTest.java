import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import utils.config;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.timeout;

public class baseTest {

    private final String url = config.getProperty("baseUrl");
    private final String browserConfig = config.getProperty("driver");
    private final int timeoutConfig = Integer.parseInt(config.getProperty("timeout"));

    WebDriver driver;

    @BeforeMethod
    public void preConfig() {
        System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

        com.codeborne.selenide.Configuration.browser = browserConfig;
        timeout = timeoutConfig;
        baseUrl = url;
        open("");
    }
}
