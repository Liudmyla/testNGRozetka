import org.junit.Assert;
import org.testng.annotations.Test;
import pages.rozetkaLoginPage;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class testNGTest extends baseTest {

    @Test(groups = {"Negative", "Login"})
    public void loginAttempt() {
        rozetkaLoginPage loginPage = new rozetkaLoginPage(getWebDriver());
        loginPage.loginPageOpen();

       //incorrect data enter
        loginPage.loginSet("testuser");
        loginPage.passSet("123");
        loginPage.submitBtnClick();
       //check that login link displayed, e.g user not entered to system
        Assert.assertTrue("User enters successfully with incorrect login, pass", loginPage.loginPageFound());
    }
}
